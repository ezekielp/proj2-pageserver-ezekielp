"""
Author: Zeke Petersen

CIS 322 Fall 2019

Project 2

This program will mimic the functionality of pageserver.py, but using Flask.
It should display existing pages with valid names and throw the appropriate
error codes/ html pages if pages are not found (404) or the file name uses
forbidden characters (403).
"""

from flask import Flask, render_template, request, abort

app = Flask(__name__, static_url_path='/static')

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/<path:path>")
def display(path):
    # Primary function for routing requests. <path:path> will capture the full
    # request (for forbidden character checking) and will be displayed if the 
    # file exists.
    if (("//" in path) or (".." in path) or ("~" in path)):
        abort(403)
    else:
        try:
            return render_template(path)
        except:
            abort(404)

@app.errorhandler(404)
def error_404(error):
    # Return the 404.html from the templates folder
    return render_template("404.html"), 404

@app.errorhandler(403)
def error_403(error):
    # Return the 403.html from the templates folder
    return render_template("403.html"), 403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
