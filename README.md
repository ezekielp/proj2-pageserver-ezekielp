# Proj2-Pageserver
------------------

Author: Zeke Petersen, ezekielp@uoregon.edu

https://bitbucket.org/ezekielp/proj2-pageserver-ezekielp

# README #

* The goal of this project is to implement the same "file checking" logic that we implemented in project 1 using flask. 

* Like project 1, if a file ("name.html") exists, transmit "200/OK" header followed by that file html. If the file doesn't exist, transmit an error code in the header along with the appropriate page html in the body. 

    * "404.html" will display "File not found!"
    * "403.html" will display "File is forbidden!"
    

# Who do I talk to? ###

* Maintained by Ram Durairajan, Steven Walton.
* Use our Piazza group for questions. Make them public unless you have a good reason to make them private, so that everyone benefits from answers and discussion. 
